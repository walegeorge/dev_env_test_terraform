resource "aws_elb" "nginx_elb" {
  name               = "Nginx-elb"
  subnets            = ["subnet-013ea4ecd484737d7"]
  security_groups    = ["aws_default_security_group.allow_https.id"]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

 # health_check {
   # healthy_threshold   = 2
   # unhealthy_threshold = 2
  #  timeout             = 3
   # target              = "HTTP:80/"
   # interval            = 30
  #}

 # tags = {
 #   Name = "Nginx-terraform-elb"
  #}
  depends_on = [
    aws_subnet.subnet1, aws_subnet.subnet2
  ]
}