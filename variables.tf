variable "availability_zones" {
  type        = list(string)
  default     = null
}

variable "my_subnets" {
  type        = list(string)
  default     = ["172.16.0.0/24","172.16.1.0/24"]

}