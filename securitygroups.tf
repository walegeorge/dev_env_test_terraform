resource "aws_default_security_group" "allow_https" {
  vpc_id      = module.vpc.vpc_id

  ingress {
    description      = "https from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    #cidr_blocks      = ["99.250.229.51/32"]
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "https from VPC"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["99.250.229.51/32"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "Allow HTTP and HTTPS"
    "Terraform" : "true"
  }
}